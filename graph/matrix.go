package graph

import (
	"image/color"
	"io"
	"strconv"
	"strings"

	"github.com/loov/plot"
	"gitlab.com/egonelbre/dnn/ssa"
)

func WriteMatrixSVG(w io.Writer, exprs []*ssa.Expr) error {
	list := ssa.FlattenNonConst(exprs)

	indexOf := map[*ssa.Expr]int{}
	lastReference := make([]int, len(list))
	for index, expr := range list {
		indexOf[expr] = index
		lastReference[index] = index
	}

	for index, expr := range list {
		for _, arg := range expr.CallArgs {
			if row, ok := indexOf[arg]; ok {
				if lastReference[row] < index {
					lastReference[row] = index
				}
			}
		}
		if row, ok := indexOf[expr.Value]; ok {
			if lastReference[row] < index {
				lastReference[row] = index
			}
		}
	}

	formatLocation := func(args []int) string {
		s := "[" + strconv.Itoa(args[0])
		for _, arg := range args[1:] {
			s += ", " + strconv.Itoa(arg)
		}
		s += "]"
		return s
	}
	var variableSymbol func(expr *ssa.Expr) string
	expressionString := func(expr *ssa.Expr) string {
		if expr.IsConst() {
			return expr.String()
		}

		switch expr.Kind {
		case ssa.KindLoad:
			return expr.Name + formatLocation(expr.Location)
		case ssa.KindCall:
			args := []string{}
			for _, arg := range expr.CallArgs {
				args = append(args, variableSymbol(arg))
			}

			if op, isOp := ssa.Operators[expr.Name]; isOp {
				return "(" + strings.Join(args, " "+op+" ") + ")"
			}

			return expr.Name + "(" + strings.Join(args, ",") + ")"
		default:
			panic("unhandled")
		}
	}
	variableSymbol = func(expr *ssa.Expr) string {
		if expr.IsConst() {
			return expr.String()
		}

		idx, ok := indexOf[expr]
		if !ok {
			if expr.Kind == ssa.KindLoad {
				return expr.Name + formatLocation(expr.Location)
			}
			return expressionString(expr)
		}
		return "v" + strconv.Itoa(idx)
	}

	const CellSize = 4
	size := float64((len(list) + 2) * CellSize)

	canvas := plot.NewSVG(size, size)
	// canvas.Rect(canvas.Bounds(), &plot.Style{Fill: color.NRGBA{0, 0, 0, 255},})
	matrix := canvas.Context(plot.R(CellSize, CellSize, size-CellSize, size-CellSize))

	computeStyle := &plot.Style{
		Fill: color.NRGBA{128, 128, 128, 255},
	}
	linkStyle := &plot.Style{
		Fill: color.NRGBA{0, 0, 0, 255},
	}
	valueStyle := &plot.Style{
		Fill: color.NRGBA{150, 100, 100, 255},
	}
	livenessStyle := &plot.Style{
		Stroke: color.NRGBA{150, 0, 0, 255},
	}

	for row, expr := range list {
		y := float64(row) * CellSize
		matrix.Rect(plot.R(y, y, y+CellSize, y+CellSize), computeStyle)

		for _, arg := range expr.CallArgs {
			if col, ok := indexOf[arg]; ok {
				x := float64(col) * CellSize
				matrix.Rect(plot.R(x, y, x+CellSize, y+CellSize), linkStyle)
			}
		}

		if col, ok := indexOf[expr.Value]; ok {
			x := float64(col) * CellSize
			matrix.Rect(plot.R(x, y, x+CellSize, y+CellSize), valueStyle)
		}
	}

	links := matrix.Layer(-1)
	for row, _ := range list {
		lastRow := lastReference[row]
		links.Poly([]plot.Point{
			plot.P(float64(row)*CellSize+CellSize/2, float64(row)*CellSize+CellSize/2),
			plot.P(float64(row)*CellSize+CellSize/2, float64(lastRow)*CellSize+CellSize/2),
		}, livenessStyle)
	}

	_, err := w.Write(canvas.Bytes())
	return err
}
