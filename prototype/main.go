package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
)

type Op byte

const (
	OpZero = Op(iota)
	OpConst
	OpLoad
	OpAdd
	OpMul
	OpPool
	OpAct
	OpStore
)

type V struct {
	Op      Op
	X, Y, Z int
	Args    []*V
	Const   float64

	Computed bool
	RefCount int
}

func (v *V) args() string {
	var strs []string
	for _, x := range v.Args {
		strs = append(strs, x.String())
	}
	return "(" + strings.Join(strs, ", ") + ")"
}
func (v *V) IsZero() bool { return v.Op == OpConst && v.Const == 0.0 }

type Cost struct {
	Load  int
	Mul   int
	Add   int
	Act   int
	NPool int
	Pool  int
}

func AddCost(a, b Cost) Cost {
	return Cost{
		Load:  a.Load + b.Load,
		Mul:   a.Mul + b.Mul,
		Add:   a.Add + b.Add,
		Act:   a.Act + b.Act,
		NPool: a.NPool + b.NPool,
		Pool:  a.Pool + b.Pool,
	}
}

func cost(v *V, recurse func(v ...*V) Cost) Cost {
	switch v.Op {
	case OpConst:
		return Cost{}
	case OpLoad:
		return Cost{Load: 1}
	case OpAdd:
		return AddCost(Cost{Add: len(v.Args) - 1}, recurse(v.Args...))
	case OpMul:
		return AddCost(Cost{Mul: len(v.Args) - 1}, recurse(v.Args...))
	case OpPool:
		return AddCost(Cost{NPool: 1, Pool: len(v.Args) - 1}, recurse(v.Args...))
	case OpAct:
		return AddCost(Cost{Act: len(v.Args)}, recurse(v.Args...))
	}
	return Cost{}
}

func TotalCost(vs ...*V) Cost {
	total := Cost{}
	for _, v := range vs {
		total = AddCost(total, cost(v, TotalCost))
	}
	return total
}

func CachedTotalCost(vs ...*V) Cost {
	total := Cost{}
	for _, v := range vs {
		v.RefCount++
		if v.Computed {
			continue
		}
		v.Computed = true
		total = AddCost(total, cost(v, CachedTotalCost))
	}
	return total
}

func (v *V) String() string {
	if len(v.Args) == 1 && (v.Op == OpAdd || v.Op == OpMul || v.Op == OpPool) {
		return v.Args[0].String()
	}

	switch v.Op {
	case OpConst:
		return fmt.Sprintf("%.3f", v.Const)
	case OpLoad:
		return "Load(" + strconv.Itoa(v.X) + "," + strconv.Itoa(v.Y) + "," + strconv.Itoa(v.Z) + ")"
	case OpStore:
		return "Store(" + strconv.Itoa(v.X) + "," + strconv.Itoa(v.Y) + "," + strconv.Itoa(v.Z) + "," + v.args() + ")"
	case OpAdd:
		return "Add" + v.args()
	case OpMul:
		return "Mul" + v.args()
	case OpPool:
		return "Pool" + v.args()
	case OpAct:
		return "Act" + v.args()
	}
	return "Z"
}

func Load(x, y, z int) *V        { return &V{Op: OpLoad, X: x, Y: y, Z: z} }
func Store(x, y, z int, e *V) *V { return &V{Op: OpStore, X: x, Y: y, Z: z, Args: []*V{e}} }
func Const(v float64) *V         { return &V{Op: OpConst, Const: v} }
func Act(a *V) *V                { return &V{Op: OpAct, Args: []*V{a}} }

func Mul(a ...*V) *V  { return &V{Op: OpMul, Args: a} }
func Add(a ...*V) *V  { return &V{Op: OpAdd, Args: a} }
func Pool(a ...*V) *V { return &V{Op: OpPool, Args: a} }

func RandomConst() *V {
	return Const(float64(rand.Intn(8)) / 8)
}

func main() {
	const DEPTH = 64
	const BLOCK = 8

	var src [BLOCK][BLOCK]*V
	k := 0
	for y := range src {
		for x := range src[y] {
			src[y][x] = Load(x, y, 0)
			k++
		}
	}

	table := NewTable()
	intern := table.Intern

	var weights [3][3][DEPTH]*V

	rand.Seed(0)
	for y := range weights {
		for x := range weights[y] {
			for k := range weights[y][x] {
				weights[y][x][k] = RandomConst()
			}
		}
	}

	var conv [BLOCK - 2][BLOCK - 2][DEPTH]*V
	var convOptimize []*V

	for y := range conv {
		for x := range conv[y] {
			for k := range conv[y][x] {
				sum := Add(
					table.Intern(Mul(src[y+0][x+0], weights[0][0][k])),
					table.Intern(Mul(src[y+1][x+0], weights[0][1][k])),
					table.Intern(Mul(src[y+2][x+0], weights[0][2][k])),

					table.Intern(Mul(src[y+0][x+1], weights[1][0][k])),
					table.Intern(Mul(src[y+1][x+1], weights[1][1][k])),
					table.Intern(Mul(src[y+2][x+1], weights[1][2][k])),

					table.Intern(Mul(src[y+0][x+2], weights[2][0][k])),
					table.Intern(Mul(src[y+1][x+2], weights[2][1][k])),
					table.Intern(Mul(src[y+2][x+2], weights[2][2][k])),
				)
				convOptimize = append(convOptimize, sum)
				conv[y][x][k] = sum
			}
		}
	}

	BinaryReduce1(convOptimize)

	for y := range conv {
		for x := range conv[y] {
			for k := range conv[y][x] {
				conv[y][x][k] = intern(Act(conv[y][x][k]))
			}
		}
	}

	const POOLSIZE = (BLOCK - 2) / 2
	var pool [POOLSIZE][POOLSIZE][DEPTH]*V
	var poolOptimize []*V

	for y := range pool {
		for x := range pool[y] {
			for k := range pool[y][x] {
				op := Pool(
					conv[y*2][x*2][k],
					conv[y*2][x*2+1][k],
					conv[y*2+1][x*2][k],
					conv[y*2+1][x*2+1][k],
				)
				poolOptimize = append(poolOptimize, op)
				pool[y][x][k] = op
			}
		}
	}

	BinaryReduce1(poolOptimize)

	for y := range pool {
		for x := range pool[y] {
			for k := range pool[y][x] {
				pool[y][x][k] = intern(pool[y][x][k])
			}
		}
	}

	var naiveCost Cost
	var sharedCost Cost
	for y := range pool {
		for x := range pool[y] {
			for k := range pool[y][x] {
				naiveCost = AddCost(naiveCost, TotalCost(pool[y][x][k]))
				sharedCost = AddCost(sharedCost, CachedTotalCost(pool[y][x][k]))
			}
		}
	}

	fmt.Printf("// Naive:  %+v\n", naiveCost)
	fmt.Printf("// Shared: %+v\n", sharedCost)

	var code Code
	code.emitted = map[*V]int{}

	for y := range pool {
		for x := range pool[y] {
			for k := 0; k < DEPTH; k++ {
				code.emitLayers(Store(x, y, k, pool[y][x][k]))
			}
		}
	}

	fmt.Println(code.b.String())
}

type Code struct {
	b       bytes.Buffer
	emitted map[*V]int
}

func (code *Code) names(vs ...*V) []string {
	xs := make([]string, len(vs))
	for i, v := range vs {
		id, ok := code.emitted[v]
		if ok {
			xs[i] = "t" + strconv.Itoa(id)
			continue
		}
		xs[i] = code.eval(v)
	}
	return xs
}

func (code *Code) emitLayers(v *V) {
	if _, ok := code.emitted[v]; ok || v.Op == OpConst {
		return
	}

	const N = 128
	var layers [N][]*V

	collected := map[*V]struct{}{}
	var collect func(v *V, depth int)
	collect = func(v *V, depth int) {
		if depth == N {
			return
		}
		if _, ok := collected[v]; ok {
			return
		}
		if _, ok := code.emitted[v]; ok {
			return
		}
		for _, child := range v.Args {
			collect(child, depth+1)
		}
		for _, child := range v.Args {
			if _, ok := collected[child]; ok {
				continue
			}
			if _, ok := code.emitted[child]; ok {
				continue
			}
			collected[child] = struct{}{}
			layers[depth] = append(layers[depth], child)
		}
	}
	collect(v, 0)

	for i := N - 1; i >= 0; i-- {
		for _, child := range layers[i] {
			code.emit(child)
		}
	}
	code.emit(v)
}

func (code *Code) emit(v *V) {
	if _, ok := code.emitted[v]; ok || v.Op == OpConst {
		return
	}
	for _, arg := range v.Args {
		code.emit(arg)
	}

	if v.Op == OpStore {
		fmt.Fprintf(&code.b, "%v;\n", code.eval(v))
		return
	}

	id := len(code.emitted) + 1
	code.emitted[v] = id

	tgt := code.names(v)[0]
	fmt.Fprintf(&code.b, "T %v = %v;\n", tgt, code.eval(v))
}

func (code *Code) eval(v *V) string {
	args := code.names(v.Args...)
	switch v.Op {
	case OpConst:
		return fmt.Sprintf("%.3ff", v.Const)
	case OpLoad:
		return "Load(" + strconv.Itoa(v.X) + "," + strconv.Itoa(v.Y) + "," + strconv.Itoa(v.Z) + ")"
	case OpStore:
		return "Store(" + strconv.Itoa(v.X) + ", " + strconv.Itoa(v.Y) + ", " + strconv.Itoa(v.Z) + ", " + args[0] + ")"
	case OpAdd:
		return "(" + strings.Join(args, " + ") + ")"
	case OpMul:
		return "(" + strings.Join(args, " * ") + ")"
	case OpPool:
		return "Pool(" + strings.Join(args, ", ") + ")"
	case OpAct:
		return "Act(" + strings.Join(args, ", ") + ")"
	}
	return "Z"
}
