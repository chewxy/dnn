package tensor

import (
	"fmt"

	"gitlab.com/egonelbre/dnn/ssa"
)

type Tensor struct {
	Shape
	Data []*ssa.Expr
}

func New(dims ...int) *Tensor {
	return NewWithShape(NewShape(dims...))
}

func NewWithShape(shape Shape) *Tensor {
	return &Tensor{
		Shape: shape,
		Data:  make([]*ssa.Expr, shape.Size),
	}
}

func (a *Tensor) Set(at []int, v *ssa.Expr) {
	index := a.Index(at)
	a.Data[index] = v
}

func (a *Tensor) Get(at []int) *ssa.Expr {
	index := a.Index(at)
	return a.Data[index]
}

func (a *Tensor) Apply(fn string, args ...*ssa.Expr) *Tensor {
	result := NewWithShape(a.Shape)
	for i, e := range a.Data {
		result.Data[i] = ssa.Call(
			fn,
			append([]*ssa.Expr{e}, args...)...,
		)
	}
	return result
}

func (a *Tensor) Add(b *Tensor) *Tensor {
	if !a.Shape.Eq(b.Shape) {
		panic("add: tensors must have same dimensions")
	}

	result := New(a.Dims...)
	for i := range a.Data {
		result.Data[i] = ssa.Add(a.Data[i], b.Data[i])
	}

	return result
}

func (a *Tensor) Multiply(b *Tensor) *Tensor {
	if a.Dims[len(a.Dims)-1] != b.Dims[0] {
		panic("incompatible " + a.String() + " x " + b.String())
	}
	split := len(a.Dims) - 1
	n := b.Dims[0]

	resultDims := append([]int{}, a.Dims[:split]...)
	resultDims = append(resultDims, b.Dims[1:]...)

	result := New(resultDims...)
	rit := result.Iterate(nil)

	aloc := append([]int{}, a.Dims...)
	bloc := append([]int{}, b.Dims...)
	for rit.Next() {
		copy(aloc, rit.Location[:split])
		copy(bloc[1:], rit.Location[split:])

		partials := []*ssa.Expr{}
		for k := 0; k < n; k++ {
			aloc[len(aloc)-1] = k
			bloc[0] = k
			partials = append(partials,
				ssa.Mul(a.Get(aloc), b.Get(bloc)),
			)
		}

		result.Set(rit.Location, ssa.Add(partials...))
	}

	return result
}

func (a *Tensor) Convolve(kernel *Tensor, strides []int) *Tensor {
	resultDims := make([]int, len(kernel.Dims))
	squeeze := []int{}

	for i, stride := range strides {
		inputDim := a.Dims[i]
		if stride == 0 {
			squeeze = append(squeeze, i)
			if inputDim != kernel.Dims[i] {
				panic(fmt.Sprintf("convolve2d: invalid kernel size [%d] = %d expected %d", i, kernel.Dims[i], inputDim))
			}
			resultDims[i] = 1
			continue
		}

		freeArea := inputDim - kernel.Dims[i]
		if freeArea < 0 {
			panic("convolve2d: kernel window too large")
		}

		steps, leftover := freeArea/stride, freeArea%stride
		if leftover != 0 {
			panic("convolve2d: kernel stride does not match exactly")
		}
		resultDims[i] = steps + 1
	}

	for i := len(strides); i < len(kernel.Dims); i++ {
		resultDims[i] = kernel.Dims[i]
	}

	result := New(resultDims...)
	split := len(strides)

	ait := a.Iterate(strides)
	// we only need to iterate up to a.width - k.width + 1
	for i := range kernel.Dims[:split] {
		ait.Dims[i] = ait.Dims[i] - kernel.Dims[i] + 1
	}
	rit := result.Iterate(nil)

	outChannels := New(kernel.Dims[split:]...)
	for i := range outChannels.Data {
		outChannels.Data[i] = ssa.Call("")
	}

	for ait.Next() {
		kit := kernel.Iterate(nil)
		for kit.Next() {
			aloc := ait.Offset(kit.Location[:split])
			partial := outChannels.Get(kit.Location[split:])
			partial.CallArgs = append(partial.CallArgs,
				ssa.Mul(a.Get(aloc), kernel.Get(kit.Location)),
			)
		}

		for _, arg := range outChannels.Data {
			if !rit.Next() {
				panic("shouldn't happen: result exhausted")
			}
			result.Set(rit.Location, ssa.Add(arg.CallArgs...))
			arg.CallArgs = nil
		}
	}

	if rit.Next() {
		panic("shouldn't happen: result not exhausted")
	}

	result.Shape = result.Shape.Squeeze(squeeze...)
	return result
}

func (a *Tensor) Pool(fn string, window []int, strides []int) *Tensor {
	dimCount := len(a.Dims)
	if dimCount != len(window) || dimCount != len(strides) {
		panic("pool: invalid window or stride length")
	}

	resultDims := make([]int, dimCount)
	for i, inputDim := range a.Dims {
		stride := strides[i]
		freeArea := inputDim - window[i]
		if freeArea < 0 {
			panic("pool: window too large")
		}

		steps, leftover := freeArea/stride, freeArea%stride
		if leftover != 0 {
			panic("pool: stride does not match exactly")
		}
		resultDims[i] = steps + 1
	}

	result := New(resultDims...)

	ait := a.Iterate(strides)
	// we only need to iterate up to a.width - k.width + 1
	for i := range window {
		ait.Dims[i] = ait.Dims[i] - window[i] + 1
	}

	windowShape := NewShape(window...)
	rit := result.Iterate(nil)
	for ait.Next() {
		win := windowShape.Iterate(nil)
		partials := []*ssa.Expr{}
		for win.Next() {
			aloc := ait.Offset(win.Location)
			partials = append(partials, a.Get(aloc))
		}

		if !rit.Next() {
			panic("shouldn't happen: result exhausted")
		}

		result.Set(rit.Location, ssa.Call(fn, partials...))
	}

	if rit.Next() {
		panic("shouldn't happen: result not exhausted")
	}

	return result
}
