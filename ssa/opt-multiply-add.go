package ssa

import "sort"

type MultiplyAdd struct {
	Combined int
}

func (MultiplyAdd) Name() string { return "MultiplyAdd" }

// MultiplyAdd a*3 + c*2 + b*3 => c * 2 + (a + b) * 3
func (s *MultiplyAdd) Run(list []*Expr) []*Expr {
	refCount := map[*Expr]int{}
	for _, expr := range list {
		for _, arg := range expr.CallArgs {
			refCount[arg]++
		}
	}

	const MinRefCount = 4
	const MinCombineCount = 0

	for _, expr := range list {
		if expr.Kind == KindCall && expr.Name == "Add" {
			type argumentMultiplier struct {
				multiplier  float64
				arguments   []*Expr
				multipliees []*Expr
			}

			multiplyBy := []argumentMultiplier{}
			include := func(multiplier float64, arg, multipliee *Expr) {
				for i := range multiplyBy {
					if multiplyBy[i].multiplier == multiplier {
						multiplyBy[i].arguments = append(multiplyBy[i].arguments, arg)
						multiplyBy[i].multipliees = append(multiplyBy[i].multipliees, multipliee)
						return
					}
				}
				multiplyBy = append(multiplyBy, argumentMultiplier{
					multiplier:  multiplier,
					arguments:   []*Expr{arg},
					multipliees: []*Expr{multipliee},
				})
			}

			for _, arg := range expr.CallArgs {
				if refCount[arg] > MinRefCount {
					continue
				}
				multiplier, multipliee, ok := extractConstMultiply(arg)
				if ok {
					include(multiplier, arg, multipliee)
				}
			}

			sort.Slice(multiplyBy, func(i, k int) bool {
				return len(multiplyBy[i].arguments) > len(multiplyBy[k].arguments)
			})

			for _, common := range multiplyBy {
				if len(common.arguments) < MinCombineCount {
					break
				}
				if !expr.HasArgs(common.arguments) {
					continue
				}
				for _, arg := range common.arguments {
					if !expr.RemoveArg(arg) {
						panic("expression should exist")
					}
				}
				expr.CallArgs = append(expr.CallArgs, Mul(Add(common.multipliees...), Const(common.multiplier)))
			}
		}
	}

	return trimZero(list)
}

func extractConstMultiply(expr *Expr) (multiplier float64, argument *Expr, ok bool) {
	if !(expr.Kind == KindCall && expr.Name == "Mul" && len(expr.CallArgs) == 2) {
		return 0, nil, false
	}

	for i, arg := range expr.CallArgs {
		if arg.Kind == KindConst {
			return arg.Const, expr.CallArgs[1-i], true
		}
	}

	return 0, nil, false
}
