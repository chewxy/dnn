package ssa

type Pass interface {
	Name() string
	Run(list []*Expr) []*Expr
}

func Optimize(roots []*Expr, passes ...Pass) []*Expr {
	list := roots
	for _, pass := range passes {
		list = pass.Run(list)
	}
	return list
}
