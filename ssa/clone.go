package ssa

func Clone(exprs []*Expr) []*Expr {
	clones := make(map[*Expr]*Expr)

	list := Flatten(exprs, nil)
	for _, expr := range list {
		clones[expr] = expr.ShallowClone()
	}

	for i := range list {
		list[i] = clones[list[i]]
		expr := list[i].ShallowClone()
		expr.Value = clones[expr.Value]
		for i, arg := range expr.CallArgs {
			expr.CallArgs[i] = clones[arg]
		}
	}

	result := make([]*Expr, len(exprs))
	for i := range result {
		result[i] = clones[exprs[i]]
	}

	return result
}
