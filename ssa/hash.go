package ssa

import (
	"fmt"
	"math"
	"os"

	"github.com/kr/pretty"
)

type Hash uint64

func (h *Hash) Int(v int)         { h.Uint64(uint64(v)) }
func (h *Hash) Float64(v float64) { h.Uint64(math.Float64bits(v)) }
func (h *Hash) Hash(v Hash)       { h.Uint64(uint64(v)) }

func (h *Hash) Uint64(v uint64) {
	t := uint64(*h)
	for k := 0; k < 64; k += 8 {
		t *= 1099511628211
		t ^= (v >> byte(k)) & 0xFF
	}
	*h = Hash(t)
}

func (h *Hash) Byte(b byte) {
	t := uint64(*h)
	t *= 1099511628211
	t ^= uint64(b)
	*h = Hash(t)
}

func (h *Hash) String(s string) {
	h.Int(len(s))
	for i := 0; i < len(s); i++ {
		h.Byte(s[i])
	}
}

type Table struct {
	ToExpr map[Hash]*Expr
	ToHash map[*Expr]Hash
}

func NewTable() *Table {
	return &Table{
		ToExpr: map[Hash]*Expr{},
		ToHash: map[*Expr]Hash{},
	}
}

func (table *Table) Intern(e *Expr) *Expr {
	hash := table.Hash(e)
	return table.ToExpr[hash]
}

func (table *Table) HashAll(exprs []*Expr) {
	for _, e := range exprs {
		table.Hash(e)
	}
}

func (table *Table) Erase(e *Expr) {
	h := table.ToHash[e]
	delete(table.ToHash, e)
	delete(table.ToExpr, h)
}

func (table *Table) Hash(e *Expr) Hash {
	if e == nil {
		return 2166136261
	}

	if h, ok := table.ToHash[e]; ok {
		return h
	}

	var h Hash = 14695981039346656037

	h.Int(int(e.Kind))
	h.Float64(e.Const)
	for _, loc := range e.Location {
		h.Int(loc)
	}
	h.Hash(table.Hash(e.Value))

	h.String(e.Name)
	h.Int(int(e.CallFlags))
	for _, arg := range e.CallArgs {
		h.Hash(table.Hash(arg))
	}

	if dup, duplicate := table.ToExpr[h]; duplicate {
		if !dup.Eq(e) {
			es := e.String()
			dups := dup.String()

			fmt.Fprintln(os.Stderr, "A:", es)
			fmt.Fprintln(os.Stderr, "B:", dups)

			if es != dups {
				pretty.Println(e)
				pretty.Println(dup)
				fmt.Println(NewTable().Hash(e))
				fmt.Println(NewTable().Hash(dup))
				panic("invalid Eq")
			}

			fmt.Fprintf(os.Stderr, "A: %#+v\n", e)
			fmt.Fprintf(os.Stderr, "B: %#+v\n", dup)
			panic("hash collision")
		}
	} else {
		table.ToExpr[h] = e
		table.ToHash[e] = h
	}

	return h
}
