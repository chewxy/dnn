#include <assert.h>
#include <math.h>
#include <stdio.h>

template <typename T, size_t X, size_t Y, size_t Z>
struct Tensor3 {
    static const size_t TotalSize = X * Y * Z;
    T data[TotalSize];

    constexpr size_t location(size_t x, size_t y, size_t z) {
        assert(0 <= x && x < X);
        assert(0 <= y && y < Y);
        assert(0 <= z && z < Z);
        return (X * Y * z) + (X * y) + x;
    }

    T &operator()(size_t x, size_t y, size_t z) {
        return data[location(x,y,z)];
    };

    void iota() {
        for(size_t x = 0; x < Y; x++) {
            for(size_t y = 0; y < Y; y++) {
                for(size_t z = 0; z < Z; z++) {
                    data[location(x,y,z)] = x + y + z;
                }
            }
        }
    }
};

template <typename T, size_t X, size_t Y, size_t Z, size_t W>
struct Tensor4 {
    static const size_t TotalSize = X * Y * Z * W;
    T data[TotalSize];

    constexpr size_t location(size_t x, size_t y, size_t z, size_t w) {
        assert(0 <= x && x < X);
        assert(0 <= y && y < Y);
        assert(0 <= z && z < Z);
        assert(0 <= w && w < W);
        return (X * Y * Z * w) + (X * Y * z) + (X * y) + x;
    };

    T &operator()(size_t x, size_t y, size_t z, size_t w) {
        return data[location(x,y,z,w)];
    };

    void iota() {
        for(size_t x = 0; x < Y; x++) {
            for(size_t y = 0; y < Y; y++) {
                for(size_t z = 0; z < Z; z++) {
                    for(size_t w = 0; w < W; w++) {
                        data[location(x,y,z,w)] = x + y + z + w;
                    }
                }
            }
        }
    }
};

template <typename T,
    size_t X, size_t Y, size_t Z,
    size_t KX, size_t KY, size_t KR>
void convolve2_110(
    Tensor3<T, X-KX+1, Y-KY+1, KR> &result,
    Tensor3<T, X, Y, Z>          &block,
    Tensor4<T, KX, KY, Z, KR>    &kernel
) {
    for(size_t y = 0; y < Y - 2; y++) {
        for(size_t x = 0; x < Y - 2; x++) {
            for(size_t d = 0; d < KR; d++) {
                T accumulator = T(0);
                for(size_t ky = 0; ky < KY; ky++){
                    for(size_t kx = 0; kx < KX; kx++){
                        for(size_t kz = 0; kz < Z; kz++){
                            accumulator += kernel(kx, ky, kz, d) * block(x + kx, y + ky, kz);
                        }
                    }
                }
                result(x, y, d) = accumulator;
            }
        }
    }
};

int main() {
    Tensor3<float, 6, 6, 3> block;
    Tensor4<float, 3, 3, 3, 64> kernel;
    Tensor3<float, 4, 4, 64> result;

    block.iota();
    kernel.iota();

    convolve2_110(result, block, kernel);

    for(size_t i = 0; i < result.TotalSize; i++)
        printf("%.0f\n", result.data[i]);
}